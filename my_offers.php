<?php
session_start();
include('database.php');
global $link;
if (!$_SESSION['user']) {
    header('Location:/site/index.php');
}

$login_user = $_SESSION['user']['login'];

$log_expr_time = 120;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Профиль</title>
    <link rel="stylesheet" type="text/css" href="/site/css/style_basket.css">
</head>

<body>
<div class="hidden" data-login="<?= $login_user ?>"></div>
<header>

    <a href="/site/main_page.php" class="logo_bar"> <img src="logo.png" alt=""></a>
    <div class="right_header">
        <ul class="mnu_top">
            <li><a href="/site/main_page.php">Главная</a></li>
            <li><a href="/site/catalog.php">Каталог</a></li>
            <li><a href="">Про нас</a></li>
        </ul>
        <?php if (!$_SESSION['user']) { ?>
            <div class="btns">
                <a href="/site/" class="btn_light">Войти</a>
                <a href="/site/register.php" class="btn_black">Зарегистрироваться</a>

            </div>
        <?php } else { ?>


            <div class="btns">
                <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login'] ?> </a>
                <a href="/site/logout.php" class="btn_logout">Выйти </a>

            </div> <?php } ?>


    </div>
</header>

<div class="off_info">
    <h2 class="inf_id"> Номер</h2>
    <h2 class="inf_price"> Итого </h2>
    <h2 class="inf_status">Статус</h2>
    <h2 class="inf_status"> Детали</h2>
    <h2 class="inf_sscc"> SSCC </h2>
</div>
<?php
$queryt = "SELECT * FROM `offer` where `client_login` = '$login_user' ORDER BY `id_offer` DESC ";
$resultst = $link->query($queryt);
global $login_user;
while ($row = $resultst->fetch_assoc()) {
    $id_off = $row["id_offer"];
    $total_price = $row["total_price"];
    $status = $row["status_offer"];
    ?>
    <div class="offer">
        <div class="of_elem">
            <div class="of_elem_in" data-id="<?php echo $id_off; ?>">
                <h2 class="id"> <?php echo $id_off; ?> </h2>
                <h2 class="price"><?php echo $total_price; ?>&#8381;</h2>
                <h2 class="status"><?php echo $status; ?></h2>
            </div>
            <div class="extended">
                <?php

                $que = "SELECT * FROM `basket` where `login_user` = '$login_user' AND `status` IS NOT NULL AND `offer_numb` = '$id_off' ";
                $res = $link->query($que);
                while ($ext = $res->fetch_assoc()) {
                    $id = $ext["id_drug"];
                    $amount = $ext["amount"];
                    $find_name = "SELECT * FROM `drug_site` where `id_drug` = '$id'";
//                    $find_name = "SELECT * FROM `drug_site` WHERE `id_drug` = (SELECT `id_drug` FROM `basket` where `login_user` = '$login_user' AND `status` IS NOT NULL AND `offer_numb` = '$id_off' )";
                    $name = ($link->query($find_name))->fetch_assoc();
                    $pict = $name["pict"];
                    $name_drug = $name["name"];
                    ?>

                    <div class="elem">
                        <div class="elem_in">
                        <div class="name"><?php echo $name_drug; ?></div>
                        <div class="amount">x<?php echo $amount; ?></div>
                    </div>
                    </div>


                <?php } ?>
            </div>

                        <?php
                        $find_off = mysqli_query($link, "SELECT * FROM `offer-code_drug` WHERE `id_off` = '$id_off'");

                        if (mysqli_num_rows($find_off)) { ?>
                            <div class="sscc_cpd"> <?php
                                $sscc_pre = "SELECT * FROM `offer-code_drug` WHERE `id_off` = '$id_off'";
                                $sscc_do = $link->query($sscc_pre);
                                while ($sscc = $sscc_do->fetch_assoc()){ ?>
                                    <div class="sscc_out"><?php echo $sscc["SCCC"]; ?></div>
                                <?php } ?></div>

                        <?php }  else {  ?>

                            <div class="sscc_out">---</div>

                        <?php }

                        ?>






        </div>
    </div>

<?php } ?>


</body>
</html>