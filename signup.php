
<?php
session_start();
include('database.php');
global $link;
if (isset($_POST['go'])) {

    $login = $_POST['login'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password_confirm'];
    $email = $_POST['email'];
    $ogrn = $_POST['ogrn'];
    $phone = $_POST['phone'];
    $apteka = $_POST['apteka'];
    $error = '';

    if (!$login || !$password || !$email || !$password_confirm || !$apteka || !$ogrn || !$phone)
    {
        $_SESSION['message'] = 'Не заполнены все поля';
        $error = 'error';
        header('Location:/site/register.php');

    }

    $check_user_reg_login = mysqli_query($link, "SELECT * FROM `client` WHERE `login` = '$login' OR `e-mail` = '$email' OR `ogrn` = '$ogrn' OR `phone` = '$phone'");

    if (mysqli_num_rows($check_user_reg_login)) {
        $_SESSION['message'] = 'Пользователь с таким логином, почтой, огрн или телефоном уже зарегистрирован';
        $error = 'Pass';
        header('Location:/site/register.php');
    }

    if ($password === $password_confirm) {
        //con...
    } else {
        $_SESSION['message'] = 'Пароли не совпадают';
        $error = 'Pass';
        header('Location:/site/register.php');
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['message'] = 'Поверьте коррекность почты';
        $error = 'Pass';
        header('Location:/site/register.php');
    }

    if(!preg_match("/^[0-9]{10,10}+$/", $_POST['phone'])) {
        $_SESSION['message'] = 'Поверьте коррекность телефона';
        $error = 'Pass';
        header('Location:/site/register.php');
    }

    if(!preg_match("/^[0-9]{13,13}+$/", $_POST['ogrn'])) {
        $_SESSION['message'] = 'Поверьте коррекность ОГРН';
        $error = 'Pass';
        header('Location:/site/register.php');
    }

    if (!$error) {
        $query = "INSERT INTO `client` (`id_client`, `ogrn`, `phone`, `login`, `password`, `drug_store_name`, `e-mail`) 
                  VALUES (NULL, '$ogrn', '$phone', '$login', '$password', '$apteka', '$email')";
        mysqli_query($link, $query);
        $_SESSION['message_alert'] = "Вы зарегистрировались. Авторизируйтесь";
        header('Location:/site');

    } else { echo 'Ошибка 1'; exit;}
}

