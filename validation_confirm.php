<?php
    session_start();
    include('database.php');
    include ('offer.php');
    global $link;
    global $res_pr;
    $err = "";
    if (isset($_POST['go_off'])) {
               function is_valid_credit_card($s) {
            // оставить только цифры
                $s = strrev(preg_replace('/[^\d]/','',$s));
                return (preg_match("/^[0-9]{16}+$/", $s));


        }

        if (!is_valid_credit_card($_POST['card_n'])) {
            $_SESSION['message'] = "Проверьте номер карты";
            $err = "card";
            header('Location:/site/offer.php');
        }


        if(!preg_match("/^[0-9]{3,3}+$/", $_POST['cvv'])) {
            $_SESSION['message'] = "Проверьте CVV";
            $err = "cvv";
            header('Location:/site/offer.php');
        }

        if ($err) {
            header('Location:/site/check_amount.php');

        }



    }