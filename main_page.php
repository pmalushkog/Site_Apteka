<?php
session_start();
$log_expr_time = 120;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}
?>


<!doctype html>
<html lang="ru">

<head>
    <title> Главная </title>
    <meta charset = "UTF-8">
    <link rel="stylesheet" type="text/css" href="/site/css/bar_style.css">

</head>

<body>

<header>


    <a href="/site/main_page.php" class="logo_bar" > <img src="logo.png"  width="55%" height="55%" alt=""></a>
    <div class="right_header">
    <ul class="mnu_top">
        <li><a href="/site/main_page.php">Главная</a> </li>
        <li><a href="/site/catalog.php">Каталог</a> </li>
        <li><a href="">Про нас</a> </li>
    </ul>
        <?php if (!$_SESSION['user']) {?>
    <div class="btns">
        <a href="/site/" class="btn_light">Войти</a>
        <a href="/site/register.php" class="btn_black">Зарегистрироваться</a>

    </div>
        <?php } else { ?>


        <div class="btns">
            <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login']?> </a>
            <a href="/site/logout.php" class="btn_logout">Выйти </a>

        </div> <?php } ?>


    </div>
</header>

<section id="main_banner" style="background-image: url('/site/box_2.png');">
    <div class="container_custom">
        <div class="inner_banner">
            <h1>Номер 1 в оптовой торговле фармацевтикой</h1>
            <a href="/site/catalog.php" class="btn btn_join ">Начать покупки</a>
        </div>
    </div>
</section>


<section id="why_us">
    <div class="container_1400">
        <h2>Почему мы?</h2>
    <div class="description">Вот причины!</div>
    <div class="row_flex">
        <div class="why_item">
            <img src="/site/ruck.png" alt="" class="img-responsive">
            <div class="name">Быстрая и простая доставка</div>
            <div class="descr">Оперативная сборка заказа и прекрасный сервис</div>
        </div>

        <div class="why_item">
            <img src="/site/pils.png" alt="" class="img-responsive">
            <div class="name">Огромный выбор</div>
            <div class="descr">Множество видов лекарственных средств в наличии</div>
        </div>

        <div class="why_item">
            <img src="/site/clock.png" alt="" class="img-responsive">
            <div class="name">Мы ценим ваше время</div>
            <div class="descr">Доставим заказ уже на следующий день</div>
        </div>

    </div>
    </div>
</section>

<footer>
    <div class="footer_up">
        <div class="container_custom">

            <div class="col-md-6">
                <div class="mnu_footer">
                    <div class="col_mnu_footer">
                        <b>Информация</b>
                        <ul>
                        <li><a href="#">Про нас</a></li>
                        <li><a href="#">Контакты</a></li>
                        <li><a href="#">Каталог</a></li>
                        </ul>
                    </div>

                    <div class="col_mnu_footer">
                        <b>Ссылки</b>
                        <ul>
                        <li><a href="">Где мы</a></li>
                        <li><a href="">Мы в соц сетях</a></li>
                        <li><a href="">Отслеживание заказов</a></li>
                        </ul>
                    </div>

                    <div class="col_mnu_footer">
                        <b>Контакты</b>
                        <ul>
                        <li><a href="">Наш адрес</a></li>
                        <li><a href="">Наша почта</a></li>
                        <li><a href="">Наш телефон</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="logo_footer">
                <a href="#"><img src="/site/logo.png" alt=""></a>
                <img class="mephi"  src="/site/mephi.png" alt="" width="20%" height="0%">
            </div>
        </div>
    </div>
</footer>

<script src="/site/data_memo.js">

</script>


</body>



</html>


