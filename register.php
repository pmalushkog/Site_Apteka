<?php
session_start();
if ($_SESSION['user']) {
    header('Location:/site/main_page.php');
}
?>


<!doctype html>
<html lang="ru">
<head>
<meta charset = "UTF-8">
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="/site/css/main.css">
</head>
<body>
<a class="logo" href="main_page.php">
    <img src="/site/logo.png">
</a>
<form action="/site/signup.php" method="post">
    <label>Логин </label> <input type="text" name="login" placeholder="Введите свой логин" autocomplete="off">
    <label>Название Аптеки </label> <input type="text" name="apteka" placeholder="Введите название аптеки" autocomplete="off">
    <label>ОГРН </label> <input type="text" name="ogrn" placeholder="Введите ОГРН" autocomplete="off">
    <label>Почта </label> <input type="text" name="email" placeholder="Введите свою почту" autocomplete="off">
    <label>Телефон </label> <input type="text" name="phone" placeholder="Формат:XXXXXXXXXX" autocomplete="off">
    <label>Пароль </label> <input type="text" name="password" placeholder="Введите свой пароль" autocomplete="off">
    <label>Подтверждение пароля </label> <input type="text" name="password_confirm" placeholder="Введите повторно свой пароль" autocomplete="off">


    <button name="go" type="submit">Зарегистрироваться</button>
    <p> Уже загерестрированы? - <a href="/site" >Авторизируйтесь</a>! </p>
      <?php
         if ($_SESSION['message']) {
             echo '<p class="msg">' . $_SESSION['message'] . '</p>';
         }
        unset($_SESSION['message']);
        ?>

    <script src="/site/data_memo.js">

    </script>

</body>
</html>



