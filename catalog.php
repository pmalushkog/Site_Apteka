<?php
session_start();
include('database.php');
global $link;

$login_user = $_SESSION['user']['login'];


$log_expr_time = 120;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    ?>
    <script> alert("Сессия истекла"); </script> <?php
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}

?>




<!doctype html>
<html lang="ru">

<head>
    <title> Главная </title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/site/css/style_catalog.css">

</head>

<body>

<header>
    <div class="hidden" data-login="<?= $login_user ?>"></div>


    <a href="/site/main_page.php" class="logo_bar"> <img src="logo.png" alt=""></a>
    <div class="right_header">
        <ul class="mnu_top">
            <li><a href="/site/main_page.php">Главная</a></li>
            <li><a href="/site/catalog.php">Каталог</a></li>
            <li><a href="">Про нас</a></li>
        </ul>
        <?php if (!$_SESSION['user']) { ?>
            <div class="btns">
                <a href="/site/" class="btn_light">Войти</a>
                <a href="/site/register.php" class="btn_black">Зарегистрироваться</a>

            </div>
        <?php } else { ?>


            <div class="btns">
                <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login'] ?> </a>
                <a href="/site/logout.php" class="btn_logout">Выйти </a>

            </div> <?php } ?>


    </div>
</header>
<div class="sucsess_msg">
    <?php
    if ($_SESSION['sucsess_msg']) {
        echo '<p class="sucsess_mesg">' . $_SESSION['sucsess_msg'] . '</p>';
    }
    unset($_SESSION['sucsess_msg']);
    ?>
</div>

<div class="jc">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

    <script>
        $('#go').on('click', function(){
            $.ajax({
                url: '/site/catalog.php',
                success: function(data) {
                    alert('Success');
                }
            });
        })
    </script>

<?php
$query = "SELECT * FROM `drug_site`";
$results = $link->query($query);

while($row = $results->fetch_assoc()) {
    $name_s = $row["name"];
//    $amount = "SELECT COUNT(*) FROM `code_drug` WHERE `drug_name` = '$name_s' ";
//    $q_amount = $link->query($amount);
//    $fq_amount = $q_amount->fetch_assoc();
//    $remains = $fq_amount['COUNT(*)'];

   ?>

        <div class="col-md">
    <div class="col-md-4">
        <div class="image">
            <img src=" <?= $row["pict"] ?>"  alt="">
        </div>
        <div class="info">
            <h3><a href="/site/description.php?id=<?=$row["id_drug"] ?>"> <?=  $row["name"] ?> </a> </h3>
            <div class="curr_amount"><?php echo $row['remains']; ?> шт.</div>
            <div>
                <span class="price"> <?= $row["price"] ?> &#8381;</span>
                <?php if ($_SESSION['user'] && $_SESSION['user']['login'] != 'admin') {?>
                <button class="add_to_cart" ><input type="hidden" name="amount" value="1" class="amount" />
                <ion-icon name="cart-outline" data-id ="<?=$row["id_drug"] ?>" ><input type="hidden" class="go_cart" name="go_cart" value="<?=$row["id_drug"] ?>" /></ion-icon></button>
                <?php } else { echo ''; }?> </button>
            </div>
        </div>
    </div>

</div>


<?php }  ?>
</div>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script
        src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU="
        crossorigin="anonymous"></script>


<script>
    document.onclick = event => {
        if (event.target.classList.contains('hydrated')) {
            var id_dValue = event.target.dataset.id;
            var amountValue = 1;
            var loginValue = $('div.hidden').data('login');

            $.ajax({
                method: "POST",
                url: "cart.php",
                data: {login_user: loginValue, id_drug: id_dValue, amount: amountValue}
            })
                // .done(function (msg) {
                //     alert("Data Saved: " + msg);
                // });

        }
    }





</script>


</body>
</html>
