<?php
session_start();
include('database.php');
global $link;

$login_user = $_SESSION['user']['login'];

$log_expr_time = 120;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}

?>

<!doctype html>
<html lang="ru">

<head>
    <title> Главная </title>
    <meta charset = "UTF-8">
    <link rel="stylesheet" type="text/css" href="/site/css/style_catalog.css">

</head>

<body>
<div class="hidden" data-login="<?=$login_user?>"></div>

<header>

    <a href="/site/main_page.php" class="logo_bar" > <img src="logo.png" alt=""></a>
    <div class="right_header">
        <ul class="mnu_top">
            <li><a href="/site/main_page.php">Главная</a> </li>
            <li><a href="/site/catalog.php">Каталог</a> </li>
            <li><a href="">Про нас</a> </li>
        </ul>
        <?php if (!$_SESSION['user']) {?>
            <div class="btns">
                <a href="/site/" class="btn_light">Войти</a>
                <a href="/site/register.php" class="btn_black">Зарегистрироваться</a>

            </div>
        <?php } else { ?>


            <div class="btns">
                <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login']?> </a>
                <a href="/site/logout.php" class="btn_logout">Выйти </a>

            </div> <?php } ?>


    </div>
</header>

<?php
session_start();

$id =  $_GET["id"];
$queryt = "SELECT * FROM `drug_site` where `id_drug` = '$id'";
$resultst = $link->query($queryt);
$rest = $resultst->fetch_assoc();
?>

<div class="container">
    <div class="main_info">
        <div class="big_img">
            <img src="<?php echo $rest["pict"]?>" alt="">
        </div>
        <div class="information">
            <h3 class="name"><?php echo $rest["name"]?></h3>
            <p>Developer: <?php echo $rest["developer"]?></p>
            <p>Остаток: <?php echo $rest["remains"]?> шт</p>
            <div class="add_block">
            <span class="price"><?php echo $rest["price"]?>&#8381;</span>
                <?php if ($_SESSION['user'] && $_SESSION['user']['login'] != 'admin') { ?>
                <div class="other_add">

                <div class="stepper">
                    <label class="stepper_field">
                        <input name ="checkin" id = "S" type="number" value= 1 maxlength="3" class="stepper_input">
                        <span class="stepper_text">шт</span>
                    </label>
                </div>
                    <div class="mesg none">Проверьте количество товара</div>
                    <button class="add_to_cart"><ion-icon name="cart-outline" data-id ="<?=$rest["id_drug"] ?>" ></ion-icon></button>
                </div>
                <?php }?>
            </div>
        </div>
    </div>

    <div class="other_info">
        <div class="instruction"><?php echo $rest["instruction"]?></div>
        <div class="developer">Something</div>
    </div>




</div>






<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script
        src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU="
        crossorigin="anonymous"></script>

<script>
    const nv = document.querySelector("input[name='checkin']");

    document.onclick = event => {

        if (event.target.classList.contains('hydrated')) {
            var id_dValue = event.target.dataset.id;
            var amountValue = document.getElementById("S").value;
            var loginValue = $('div.hidden').data('login');

            $.ajax({
                method: "POST",
                url: "cart.php",
                data: {login_user: loginValue, id_drug: id_dValue, amount: amountValue},

                success(data_2) {
                    if (data_2.search("true") == -1) {
                        $('.mesg').removeClass('none');
                    } else {
                        $('.mesg').addClass('none');
                    }
                }
            });

        }
    }
</script>

</body>
</html>
