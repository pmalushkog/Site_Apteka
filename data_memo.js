let formData = {};
let form =document.querySelector('form');
let LS = localStorage;



form.addEventListener('input', function(event) {

    formData[event.target.name] = event.target.value;
    LS.setItem('formData', JSON.stringify(formData));

});

if(LS.getItem('formData')) {
    formData = JSON.parse(LS.getItem('formData'));
    
    for (let key in formData){
        form.elements[key].value = formData[key];

     }
}
