<?php
session_start();
include('database.php');
global $link;

$login_user = $_SESSION['user']['login'];

$log_expr_time = 120;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}

?>

<?php
$check_card = "SELECT COUNT(*) from `basket` where `login_user` = '$login_user' AND `status` IS NULL";
$qcheck_card = ($link->query($check_card))->fetch_assoc();
$count_check_card = $qcheck_card["COUNT(*)"];

if ($count_check_card == 0){
    header('Location:/site/profile.php');
}
?>

<!doctype html>
<html lang="ru">

<head>
    <title> Главная </title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/site/css/style_offer.css">

</head>

<body>

<header>
    <div class="hidden" data-login="<?= $login_user ?>"></div>


    <a href="/site/main_page.php" class="logo_bar"> <img src="logo.png" alt=""></a>
    <div class="right_header">
        <ul class="mnu_top">
            <li><a href="/site/main_page.php">Главная</a></li>
            <li><a href="/site/catalog.php">Каталог</a></li>
            <li><a href="">Про нас</a></li>
        </ul>
        <?php if (!$_SESSION['user']) { ?>
            <div class="btns">
                <a href="/site/" class="btn_light">Войти</a>
                <a href="/site/register.php" class="btn_black">Зарегистрироваться</a>

            </div>
        <?php } else { ?>


            <div class="btns">
                <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login'] ?> </a>
                <a href="/site/logout.php" class="btn_logout">Выйти</a>

            </div> <?php } ?>


    </div>
</header>
<div class="alert_msg">
    <?php
    if ($_SESSION['amount_msg']) {
        echo '<p class="amount_msg">' . $_SESSION['amount_msg'] . '</p>';
    }
    unset($_SESSION['amount_msg']);
    ?>
</div>

<h3 class="name-offer">Ваш заказ:</h3>
<div class="tags">
    <h2>Товар</h2>
    <h2>Количество</h2>
    <h2>Цена</h2>
</div>

<?php
$que = "SELECT * FROM `basket` where `login_user` = '$login_user' AND `basket`.`status` IS NULL";
$res = $link->query($que);
$res_pr = 0;

while ($row = $res->fetch_assoc()) {
    $id = $row["id_drug"];
    $find_name = "SELECT * FROM `drug_site` where `id_drug` = '$id'";
    $name = ($link->query($find_name))->fetch_assoc();

    ?>
    <div class="position">
        <div class="<?php $id ?>">
            <div class="one_p">
                <div class="image">
                    <img src="<?php echo $name["pict"]; ?>" alt="">
                </div>
                <div class="info">
                    <div class="pr_name">
                        <h2><a href="/site/description.php?id=<?= $row["id_drug"] ?>"
                               class="name_drug"> <?php echo $name["name"]; ?> </a></h2>
                    </div>
                </div>
                <div class="amount"><?php echo $row["amount"]; ?> шт.</div>
                <div class="total_price"><?php echo($row["amount"] * $name["price"]);
                    $res_pr += $row["amount"] * $name["price"]; ?>&#8381;
                </div>
            </div>
        </div>
    </div>


<?php } ?>

<div class="line"></div>
<div class="res_price">Итого: <?php echo $res_pr; ?> </div>
<div class="form_offer">
    <div class="enter_data">
        <h2>Заполните данные заказа:</h2>
    </div>
    <div class="of_data">
        <form action="/site/validation_confirm.php" method="post">
        <label></label><input name = "adress" type="text" placeholder="Адрес достваки: город, улица, дом" autocomplete="off">
        <label></label><input name ="card_n" type="text" placeholder="Введите номер карты" autocomplete="off">
        <label></label><input  type="text" name = "cvv" placeholder="Введите cvv код" autocomplete="off">




            <button name="go_off" class="go_off">Отправить</a></button>
    </div>
    <?php
    if ($_SESSION['message']) {
        echo '<p class="msg">' . $_SESSION['message'] . '</p>';
    }
    unset($_SESSION['message']);
    ?>



</div>

</body>
</html>
