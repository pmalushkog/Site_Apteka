<?php
session_start();
if ($_SESSION['user']) {
    header('Location:/site/main_page.php');
}
?>


<!doctype html>
<html lang="ru">
<head>
    <meta charset = "UTF-8">
    <title>Авторизация</title>
    <link rel="stylesheet" type="text/css" href="/site/css/main.css">
</head>

<body>
<?php

if ($_SESSION['message_alert']) {
    echo '<p class="msg_alrt">' . $_SESSION['message_alert'] . '</p>';
    unset($_SESSION['message_alert']);
}
?>


<a class="logo_1" href="/site/main_page.php" >
<img src="/site/logo.png ">
</a>

<form action="/site/signin.php" method="post">
    <label>Логин </label> <input type="text" name="login" placeholder="Введите свой логин" autocomplete="off">
    <label>Пароль </label> <input type="password" name="password" placeholder="Введите свой пароль" autocomplete="off">

    <button name="go_auth" type="submit">Войти</button>
    <p> Нет аккаутна - <a href="/site/register.php" >зарегистрируйтесь</a>! </p>

    <?php
    if ($_SESSION['message']) {
        echo '<p class="msg">' . $_SESSION['message'] . '</p>';
    }
    unset($_SESSION['message']);
    ?>
    <script src="/site/data_memo.js">

    </script>
</body>
</html>