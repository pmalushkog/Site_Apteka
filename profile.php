<?php
session_start();
include('database.php');
global $link;
if (!$_SESSION['user']) {
    header('Location:/site/index.php');
}

$login_user = $_SESSION['user']['login'];

$log_expr_time = 120;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Профиль</title>
    <link rel="stylesheet" type="text/css" href="/site/css/style_basket.css">
</head>

<body>
<div class="hidden" data-login="<?= $login_user ?>"></div>
<header>

    <a href="/site/main_page.php" class="logo_bar"> <img src="logo.png" alt=""></a>
    <div class="right_header">
        <ul class="mnu_top">
            <li><a href="/site/main_page.php">Главная</a></li>
            <li><a href="/site/catalog.php">Каталог</a></li>
            <li><a href="">Про нас</a></li>
        </ul>
        <?php if (!$_SESSION['user']) { ?>
            <div class="btns">
                <a href="/site/" class="btn_light">Войти</a>
                <a href="/site/register.php" class="btn_black">Зарегистрироваться</a>

            </div>
        <?php } else { ?>


            <div class="btns">
                <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login'] ?> </a>
                <a href="/site/logout.php" class="btn_logout">Выйти</a>

            </div> <?php } ?>


    </div>
</header>
<?php if ($login_user != 'admin') {?>
<div class="basket_desc">
    <h2> <?= $_SESSION['user']['login'] ?> </h2>
    <h2><a href="/site/my_offers.php" class="my_offers">Мои заказы</a></h2>
    <h3>Корзина</h3>
</div>
<?php } else { ?>
    <div class="basket_desc">
        <h2> <?= $_SESSION['user']['login'] ?> </h2>
        <h2><a href="/site/clients_offers.php" class="my_offers">Заказы клиентов</a></h2>

    </div>

<?php } ?>


<?php
$queryt = "SELECT * FROM `basket` where `login_user` = '$login_user' AND `basket`.`status` IS NULL";
$resultst = $link->query($queryt);


while ($row = $resultst->fetch_assoc()) {
    $id = $row["id_drug"];
    $find_name = "SELECT * FROM `drug_site` where `id_drug` = '$id'";
    $name = ($link->query($find_name))->fetch_assoc();

    ?>

<div class = "<?php echo$id?>">
    <div class="position">
        <div class="<?php $id ?>">
        <div class="one_p">
            <div class="image">
                <img src="<?php echo $name["pict"]; ?>" alt="">
            </div>

            <div class="info">
                <div class="pr_name">

                    <h2><a href="/site/description.php?id=<?= $row["id_drug"] ?>"
                           class="name_drug"> <?php echo $name["name"]; ?> </a></h2>

                </div>
            </div>

            <div class="amount">
                <div class="stepper">
                    <label class="stepper_field">
                        <input name ="checkin" id="S" type="number" value="<?php echo $row["amount"] ?>" maxlength="3"
                               class="stepper_input" data-id = "<?php echo $id ?>" onchange="update(this)">
                        <span class="stepper_text">шт</span>
                    </label>
                </div>
            </div>
            <button class="btn_clear" data-id="<?= $row["id_drug"] ?>">X</button>
        </div>
        </div>

    </div>
</div>

<?php } ?>

<div class="btn_logout_m"><a href="/site/logout.php" class="logout">Выход</a></div>

<button name="btn_offer" type="submit" class="btn_offer" ><a href="/site/offer.php">Оформить заказ</a></button>


<script
        src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU="
        crossorigin="anonymous"></script>

<script>

    function update(c) {
        var id_dValue = c.dataset.id;
        var amountValue = c.value;
        var loginValue = $('div.hidden').data('login');
        alert(loginValue);
        $.ajax({
            method: "POST",
            url: "update_cart.php",
            data: {login_user: loginValue, id_drug: id_dValue, amount: amountValue}
        });
    }

    document.onclick = event => {
        if (event.target.classList.contains('btn_clear')) {
            var id_dValue = event.target.dataset.id;
            var loginValue = $('div.hidden').data('login');
            $.ajax({
                    url: 'dis_cart.php',
                    type: 'POST',
                    data: {
                        login_user: loginValue, id_drug: id_dValue
                    },
                    success(data) {
                        $('.' + id_dValue).addClass('none');
                    }

                }
            );
        }
    }


</script>


</body>
</html>