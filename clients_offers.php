<?php
session_start();
include('database.php');
global $link;
if (!$_SESSION['user']) {
    header('Location:/site/index.php');
}



$login_user = $_SESSION['user']['login'];


$log_expr_time = 10;
if (isset($_SESSION['log_start']) && time() - $_SESSION['log_start'] > $log_expr_time) {
    header('Location:/site/logout.php');
} elseif (isset($_SESSION['user'])) {
    $_SESSION['log_start'] = time();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Профиль</title>
    <link rel="stylesheet" type="text/css" href="/site/css/style_basket.css">
</head>

<body>
<div class="hidden" data-login="<?= $login_user ?>"></div>
<header>

    <a href="/site/main_page.php" class="logo_bar"> <img src="logo.png" alt=""></a>
    <div class="right_header">
        <ul class="mnu_top">
            <li><a href="/site/main_page.php">Main</a></li>
            <li><a href="/site/catalog.php">Catalog</a></li>
            <li><a href="">About us</a></li>
        </ul>
        <?php if (!$_SESSION['user']) { ?>
            <div class="btns">
                <a href="/site/" class="btn_light">Sign in</a>
                <a href="/site/register.php" class="btn_black">Sign up</a>

            </div>
        <?php } else { ?>


            <div class="btns">
                <a href="/site/profile.php" class="btn_profile"><?php echo $_SESSION['user']['login'] ?> </a>
                <a href="/site/logout.php" class="btn_logout">Logout </a>

            </div> <?php } ?>


    </div>
</header>

<div class="off_info">
    <h2 class="inf_id"> Номер</h2>
    <h2 class="inf_price"> Итого </h2>
    <h2 class="inf_status">Статус</h2>
    <h2 class="inf_status"> Детали</h2>
    <h2 class="inf_st_cpd">Состояние</h2>
    <h2 class="inf_sscc"> SSCC </h2>
    <h2 class="inf_login">Клиент</h2>
</div>

<?php
$queryt = "SELECT * FROM `offer` ORDER BY `id_offer` DESC ";
$resultst = $link->query($queryt);
global $login_user;
while ($row = $resultst->fetch_assoc()) {
    $id_off = $row["id_offer"];
    $total_price = $row["total_price"];
    $status = $row["status_offer"];

    ?>
    <div class="offer">
        <div class="of_elem">
            <div class="of_elem_in" data-id="<?php echo $id_off; ?>">
                <h2 class="id"> <?php echo $id_off; ?> </h2>
                <h2 class="price"><?php echo $total_price; ?>&#8381;</h2>
                <h2 class="status"><?php echo $status; ?></h2>
            </div>
            <div class="extended">
                <?php

                $que = "SELECT * FROM `basket` WHERE `status` IS NOT NULL AND `offer_numb` = '$id_off'";
                $res = $link->query($que);
                while ($ext = $res->fetch_assoc()) {
                    $id = $ext["id_drug"];
                    $amount = $ext["amount"];
                    $find_name = "SELECT * FROM `drug_site` where `id_drug` = '$id'";
                    $name = ($link->query($find_name))->fetch_assoc();
                    $pict = $name["pict"];
                    $name_drug = $name["name"];
                    ?>

                    <div class="elem">
                        <div class="elem_off_t">
                        <div class="name"><?php echo $name_drug; ?></div>
                        <div class="amount">x<?php echo $amount; ?></div>
                    </div>
                    </div>

                <?php } ?>
                    </div>

                        <?php if ($_SESSION['user']['login'] == 'admin') { ?>
                            <?php $find_off = mysqli_query($link, "SELECT * FROM `offer-code_drug` WHERE `id_off` = '$id_off'");

                            if (!mysqli_num_rows($find_off)) { ?>

                                <div class="<?php echo $row["id_offer"]; ?>">
                                    <div class="row_t">
                                        <button class="btn_compile" data-id="<?php echo $row["id_offer"]; ?>" data-login="<?php echo $row["client_login"];  ?>">Собрать заказ</button>
                                        <div class="sscc_out_cl">---</div>
                                    </div>
                                </div>

                            <?php } else { ?>
                                <div class="completed">Заказ собран</div>
                                <div class="sscc_cpd"> <?php
                                    $sscc_pre = "SELECT * FROM `offer-code_drug` WHERE `id_off` = '$id_off'";
                                    $sscc_do = $link->query($sscc_pre);
                                    while ($sscc = $sscc_do->fetch_assoc()){ ?>
                                        <div class="sscc_out_cl"><?php echo $sscc["SCCC"]; ?></div>

                                    <?php } ?></div>

                            <?php } ?>


                            <div class="compd_<?php echo $row["id_offer"]; ?> none">  <div class="compd_h">Заказ собран</div></div>
                            <div class="sscc_out_h_<?php echo $row["id_offer"];?> none"><div class="sscc_h"></div></div>

                        <?php } ?>

                        <div class="login_u"><?php echo $row["client_login"]; ?></div>
            <?php
            $find_phone = "SELECT `phone` FROM `client` WHERE `login` = (SELECT `client_login` FROM `offer` WHERE `id_offer` = '$id_off')";
            $qfind_phone = ($link->query($find_phone))->fetch_assoc();
            $phone = $qfind_phone["phone"];
            ?> <div class="phone"><?php echo $phone;?></div>






        </div>
    </div>

<?php } ?>

<script
    src="https://code.jquery.com/jquery-3.6.3.min.js"
    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU="
    crossorigin="anonymous"></script>

<script>
    document.onclick = event => {

        if (event.target.classList.contains('btn_compile')) {
            var idValue = event.target.dataset.id;
            var loginValue = event.target.dataset.login;

            $.ajax({
                    url: 'compile_order.php',
                    type: 'POST',
                    data: {
                        id: idValue, login_user: loginValue
                    },
                    success(data) {
                        $('.' + idValue).addClass('none');
                        $('.compd_' + idValue).removeClass('none');
                        $('.sscc_out_h_' + idValue).removeClass('none');

                    }

                }
            );
        }
    }
</script>


</body>
</html>
